module Translatable
  extend ActiveSupport::Concern

  included do
    before_save :set_translated_fields
  end

  private
    def set_translated_fields
      translated_fields.each do |field|
        self.send("translated_#{field}=", translate(send(field))) if send("#{field}_changed?")
      end
    end

    def translate(text)
      resp = client.translate_text(
        text: text,
        source_language_code: 'en',
        target_language_code: 'de',
      )

      resp.translated_text
    end

    def client
      @client ||= Aws::Translate::Client.new(
        access_key_id: ENV['AWS_ACCESS_KEY_ID'],
        secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      )
    end
end
