class Article < ApplicationRecord
  include Translatable

  has_many :questions, inverse_of: :article, dependent: :delete_all

  accepts_nested_attributes_for :questions, reject_if: :all_blank, allow_destroy: true

  validates :title, presence: true
  validate :must_have_at_least_one_question

  private
    def must_have_at_least_one_question
      errors.add(:base, 'The article must have at least one question') if questions.empty?
    end

    def translated_fields
      %i[title body]
    end
end
