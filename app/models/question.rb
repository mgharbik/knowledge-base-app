class Question < ApplicationRecord
  include Translatable

  belongs_to :article

  validates :question, presence: true
  validates :answer, presence: true

  private
    def translated_fields
      %i[question answer]
    end
end
