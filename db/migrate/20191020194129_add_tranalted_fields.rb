class AddTranaltedFields < ActiveRecord::Migration[6.0]
  def change
    add_column :articles, :translated_title, :string
    add_column :articles, :translated_body, :text
    add_column :questions, :translated_question, :text
    add_column :questions, :translated_answer, :text
  end
end
