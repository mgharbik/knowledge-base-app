class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.text :question, null: false
      t.text :answer, null: false
      t.references :article, null: false, foreign_key: true

      t.timestamps
    end
  end
end
