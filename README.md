# Knowledge Base App

This knowledge base application translates your articles from english to german.

## Requirements

- Ruby 2.6.3
- Rails 6.0.0
- Yarn

## Setup

```
$ git clone git@bitbucket.org:mgharbik/knowledge-base-app.git
$ cd knowledge-base-app
$ bundle install
$ yarn install
$ rails db:create db:migrate
$ mv .env.sample .env (Please set your valid AWS credentials)
$ rails s
```

Then visit http://localhost:3000
